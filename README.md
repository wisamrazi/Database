# Project_34

## Instructions for review

1. First you need to have a virtual environment (as shown how to set up in "Assignment_3.pdf" under the section "Part A")
   and download the git project as a zip-file and unpack it so that you can activate the
   python files in the virtual environment.

2. open the XAMPP Control Panel and start Apache and MySQL.

3. open the database by going to <http://localhost/phpmyadmin/> and make a new database
   called ski_project, then go to import and choose the newly unpacked ski_projectWithData.sql

4. open a terminal window and go to the folder where you now have all the unpacked files
   from the git project and can run them in the virtual environment. Activate the virtual
   environment and run the file endpoints.py

## Endpoints

(all urls must start with http://127.0.0.1:5000 (or whatever your terminal says))

(list of users with usernames and passwords are in test_user_info.txt)

### Public endpoint

#### /get_product_type?model=Active&min_size=152&max_size=172

This GET function shows you all the ski-models in the product table. model, min_size and
max_size are all optional filters you can add in the url to not get owerwhelmed with
too many results.

### Transporter endpoint

/get_ready_orders
This GET function shows you all orders that have the "ready to be shipped" status

json body:

```
{
	"username":"Bring1",
	"password":"password"
}
```

#### /get_ready_shipments?driver_id=1

This GET function shows you all shipments that match the driver_id in the url and
have the status "ready" or "picked up".

json body:

```
{
	"username":"Bring1",
	"password":"password"
}
```

#### /pickup_shipment

This POST function updates the status of a shipment that is "ready" and sets it to "picked up"
and updates the comment. It also updates the orders that are included in the shipment and sets
their status to "shipped". Both the nr and driver_id must match for the status to be updated.
The comment can be updated even if the status is already "picked up". If the comment is
"", it wil be set to NULL.

json body:

```
{
    "username":"Bring1",
    "password":"password",
    "nr":1,
    "driver_id":1,
    "comment":""
}
```

#### /delivered_shipment

This POST function updates the status of a shipment that is "picked up" and sets it to "delivered"
and updates the comment. Both the nr and driver_id must match for the status to be updated.
If the comment is "", it wil be set to NULL.

json body:

```
{
    "username":"Bring1",
    "password":"password",
    "nr":2,
    "driver_id":2,
    "comment":""
}
```

### Customer_rep endpoints

Endpoints for the customer_rep employees

#### /customer/rep/retrieve{?status='new' or 'skis availabe'}

Method: GET
Endpoint to retrieve all information of an order with status 'new' or status 'skis available'
The status query is optional. If no status is specified it default to 'new'

json body:

```
{
	"username": "Andreas1",
	"password": "ikkepassword"
}
```

Username needs to match an existing employee user and password needs to match that users unhashed password.

#### /customer/rep/changeState

Method: POST
Endpoint to change the state of an order from 'new' to 'skis available' for a specified order.

json body:

```
{
	"username": "Andreas1",
	"password": "ikkepassword",
	"nr": 6
}
```

Username needs to match an existing employee user and password needs to match that users unhashed password.
nr needs to be the number of an existing order.

#### /customer/rep/createshipment

Method: POST
Endpoint to make a new shipment for a specified order that has state 'ready to be shipped'
The endpoint takes data from the given order and if there are no problems makes a new shipment for it.
Lastly it updates the original order shipment nr, to match the new shimpent nr.

json body:

```
{
    "username": "Andreas1",
    "password": "ikkepassword",
    "order_nr": 3,
    "shipment_nr": 4,
    "adress": "Adresseveien 13",
    "pickup_date": "2022-03-20",
    "driver_id": 5,
    "company": "Bring",
    "comment": "Random comment"
}
```

Username needs to match an existing employee user and password needs to match that users unhashed password.
order_nr needs to be the number of an existing order.
shipment_nr needs to be a new unique number.
adress can be whatever you want.
pickup_date must be a date of YYYY-MM-DD format
driver_id can be any int
company must be an existing transport-company name
comment can be anything.

### The Customer endpoint

#### /get_production_plan?from_date = (date you choose)

This endpoint will retrieve all the production plans that have been issued 4 weeks back in time from a date chosen by the customer. This endpoint is using the GET method, so it’s not required for the customer to write anything in the Json body. The customer must write a valid date and with this format “yyyy-mm-dd”. 

json body:

```
{
   "username":"XXL1",
   "password":"password"
}
```

#### /delete_order

This endpoint will allow the customer to delete an order based on the order number that the user choses. The method this endpoint uses is the POST method, so the customer must input the following in the json body in postman to delete the chosen order.

json body:

```
{
   "username":"XXL1",
   "password":"password",
   "nr": "1"
}
```


#### /create_new_order

This endpoint creates a new order that is added to the order_skis table. It uses the POST method.

json body:

```
{
   "username":"XXL1",
   "password":"password",
   "nr": 1,
   "Order_date": "2022-11-21",
   "franchise_id": 2,
   "individual_store_id": null,
   "team_skier_id": null,
   "state": "new",
   "comment": "blablabla"
}
```

Here it is important to note that the customer has to only insert one of the ids in either "franchise_id", "individual_store_id", "team_skier_id". If the customer inserts more than 1 of these ids there, will be an error message for the user that says the following:
" ERROR, Only ONE of the following attributes must have an value: `franchise_id`,`individual_store_id`,`team_skier_id`"

#### /get_orders_since?date=(yyyy-mm-dd)

This endpoint will retrieve all the orders from a specific date and retrieves all the orders since that that date. This endpoint uses the GET method, the customer is required to type in the right date in the URL to get the order since that date. The date must be written in the following format: “yyyy-mm-dd”. 

json body:

```
{
   "username":"XXL1",
   "password":"password"
}
```

#### /get_the_order?the_id=(“the order nr you want to see”)

This endpoint will retrieve a specific order based on its order_nr. This endpoint is using the GET method, the customer is required to type in the correct order_nr in the URL. Example
/get_the_order?the_id=1

json body:

```
{
   "username":"XXL1",
   "password":"password"
}
```

#### /creating_unassigned_items
Creating a new order that contains the reference_order_nr to an already existing order. Then set the order it is referncing to as "ready-for-shipping" as status. This endpoint is using HTTP POST request

json body:

```
{
        "username": "XXL1",
        "password": "password",
        "nr": 10,
        "Order_date": "2022-11-12",
        "franchise_id": 2,
        "individual_store_id": None,
        "team_skier_id": None,
        "state": "new",
        "comment": "blablabak",
        "reference_order_nr": 2,
    }
```

### Prodution_planner_Endpoint

#### /create_productionPlan
 Creating a new prodution plan in the prodution. This endpoint is using the HTTP POST request.

 json body:

 ```
 {
        "username": "Espen1",
        "password": "password",
        "id": 556,
        "start_date": "2022-12-11"
}

 ```


### StorekeeperEndpoint

#### /get_state

This endpoint will retrieve all the orders that has the state “skis available”. The endpoint is using the GET method and there is nothing required for the user to type something specific in the URL.

json body:

```
{
   "username":"Andreas1",
   "password":"ikkepassword",
}
```

#### /change_state_ready

This endpoint will change the state of an order from “skis available” to “ready to be shipped” for a specific order number. This endpoint is using the POST method.

json body:

```
{
   "username":"Andreas1",
   "password":"ikkepassword",
   "nr": "1"
}

```


#### /storekeeper_comment

This endpoint will add a comment to an order. The reason for adding the comment is to create a transion record for orders when more skis are assigned, we are implementing this with a comment attribute. This endpoint is using the POST method.

json body:

```
{
   "username":"Andreas1",
   "password":"ikkepassword",
   "nr": "1",
   "comment": "kommentar"
}
```

#### /get_the_orders

This endpoint retriever all the order from the orders_skis table. This endpoint is using the GET method.

json body:

```
{
   "username":"Andreas1",
   "password":"ikkepassword",
}
```

#### /storekeeper/create

This endpoint will create a new record for a new product in the product table. This endpoint is using the POST method.

json body:

```
{
   "username":"Andreas1",
   "password":"ikkepassword",
   "id": 14,
   "model": "active",
   "type": "skate",
   "size": 152,
   "photourl": "google.com/image",
   "description":"blabal",
   "historical": "1"
}
```
