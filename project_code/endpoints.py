from datetime import datetime
import hashlib
from flask import Flask, request, jsonify
from flask_mysqldb import MySQL

app = Flask(__name__)

app.config['MYSQL_HOST'] = "localhost"
app.config['MYSQL_USER'] = "root"
app.config['MYSQL_PASSWORD'] = ""
app.config['MYSQL_DB'] = "ski_project"

mysql = MySQL(app)
#####################################################################################################
# Root endpoint


@app.route('/')
def index():
    return jsonify("No functionality on root level")

# eksempel url /get_product_type?model=Active&size=142


@app.route('/get_product_type', methods=['GET'])
def get_product_name():

    # check if method is get
    if request.method == 'GET':

        cur = mysql.connection.cursor()

        # sets model and sizes to be blank if there are no queries, if there are queries they are set to match them
        model = ''
        if request.args.get("model"):
            model = request.args.get("model")
        min_size = ''
        if request.args.get("min_size"):
            min_size = request.args.get("min_size")
        max_size = ''
        if request.args.get("max_size"):
            max_size = request.args.get("max_size")

        # get all skis matching the filter, if there are no filters it defaults to show all.
        product_name = cur.execute("SELECT * FROM `product` WHERE `model`= IF (%s = '', `model`, %s) AND `size`BETWEEN (IF (%s = '', `size`, %s)) AND (IF (%s = '', `size`, %s))", ([
                                   model], [model], [min_size], [min_size], [max_size], [max_size]))

        if product_name > 0:
            product_name = cur.fetchall()

        cur.close()
        return jsonify(product_name), 200

    else:
        # if method is not get
        return jsonify("Method is not supported"), 405
#####################################################################################################

#####################################################################################################
# Customer rep endpoints

# endpoints that lets customer rep retrieve information about orders that have status 'new' or 'skis available'
# example url: /customer/rep/retrieve


@app.route('/customer/rep/retrieve')
def retrieve_new_orders():

    # check if correct method is used
    if request.method == 'GET':

        # check if user has correct username and password
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            # set status and check if status is new or skis available
            status = 'new'
            statusGiven = False
            if request.args.get("status"):
                statusGiven = True
                status = request.args.get("status")
            if statusGiven == True:
                if status != 'new' and status != 'skis available':
                    return jsonify("status, needs to be 'either' new or 'skis available'"), 400

            get_new_orders = cur.execute(
                "SELECT * FROM `order_skis` WHERE `state`= %s", [status],)

            if get_new_orders > 0:
                get_new_orders = cur.fetchall()

            cur.close()
            return jsonify(get_new_orders), 200
        else:
            return jsonify("Wrong username or password"), 401
    else:
        # if method is not get
        return jsonify("Method is not supported"), 405

# endpoint that lets customer rep change a order state from 'new' to 'skis available'
# example url: /customer/rep/changeState


@app.route('/customer/rep/changeState', methods=['POST'])
def change_order_state():

    # check if method is post
    if request.method == 'POST':
        # read data from json body
        data = request.get_json()
        username = data['username']
        password = data['password']
        nr = data['nr']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            control_input = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr` = %s AND `state`= 'new'", (nr,))
            if control_input == 0:
                return jsonify("Invalid request, check if order number exists, or if it is has another state than new"), 400

            update_order_state = cur.execute(
                "UPDATE `order_skis` SET `state`= 'open' WHERE `nr`= %s", (nr,))
            mysql.connection.commit()

            change_info = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr` = %s AND `state`= 'open'", (nr,))

            if change_info > 0:
                change_info = cur.fetchall()

            cur.close()
            return jsonify(change_info), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        # if method is not get
        return jsonify("Method is not supported"), 405

# endpoint that lets customer rep create a new shipment for a order that is in the 'ready to be shipped' state
# example url: /customer/rep/createshipment


@app.route('/customer/rep/createshipment', methods=['POST'])
def create_shipment():
    # check if method is post
    if request.method == 'POST':

        # read data from json body
        data = request.get_json()

        username = data['username']
        password = data['password']
        order_nr = data['order_nr']
        shipment_nr = data['shipment_nr']
        adress = data['adress']
        pickup_date = data['pickup_date']
        driver_id = data['driver_id']
        company = data['company']
        comment = data['comment']

        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            if comment == "":
                comment = None

            cur = mysql.connection.cursor()

            # get all data based on input order_nr
            get_order = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr` = %s", (order_nr,))

            if get_order > 0:
                get_order = cur.fetchall()

            # check if order status is set to "ready to be shipped"
            if get_order[0][5] != "ready to be shipped":
                return jsonify("Cannot create shipment for orders that are not ready to be shipped"), 400

            # check number of customers tied to an order, this has to be exactly 1 for everything to function correctly
            # gets the name of the customer from the correct table
            numberOfNull = 0
            name = None
            if get_order[0][2] != None:
                numberOfNull += 1
                get_name = cur.execute(
                    "SELECT `name` FROM `franchise` WHERE `id` = %s", (get_order[0][2],))
                if get_name > 0:
                    get_name = cur.fetchall()
                name = get_name[0][0]
            if get_order[0][3] != None:
                numberOfNull += 1
                get_name = cur.execute(
                    "SELECT `name` FROM `individual_store` WHERE `id` = %s", (get_order[0][3],))
                if get_name > 0:
                    get_name = cur.fetchall()
                name = get_name[0][0]
            if get_order[0][4] != None:
                numberOfNull += 1
                get_name = cur.execute(
                    "SELECT `name` FROM `team_skier` WHERE `id` = %s", (get_order[0][4],))
                if get_name > 0:
                    get_name = cur.fetchall()
                name = get_name[0][0]

            if numberOfNull > 1:
                return jsonify("There is a problem with the selected order: orders and shipments cannot belong to multiple customers at the same time"), 400
            elif numberOfNull < 1:
                return jsonify("The selected order does not belong to any customer"), 400

            # create the new shipment based on input data and data from get_order
            post_shipment = cur.execute("INSERT INTO `shipment`(`nr`, `name`, `shipping_address`, `pickup_date`, `driver_id`, `state`, `company`, `team_skier_id`, `individual_store_id`, `franchise_id`, `comment`) VALUES (%s, %s, %s, %s, %s, 'ready', %s, %s, %s, %s, %s)", (
                shipment_nr, name, adress, pickup_date, driver_id, company, get_order[0][4], get_order[0][3], get_order[0][2], comment,))
            mysql.connection.commit()

            # get all data in the new shipment
            shipment_info = cur.execute(
                "SELECT * FROM `shipment` WHERE `nr` = %s", (shipment_nr,))

            if shipment_info > 0:
                shipment_info = cur.fetchall()

            # update shipment nr in order to match the new shipment number
            update_order_shipment = cur.execute(
                "UPDATE `order_skis` SET `shippment_nr` = %s WHERE `nr`= %s", (shipment_nr, order_nr,))
            mysql.connection.commit()

            cur.close()
            return jsonify(shipment_info)
        else:
            return jsonify("Wrong username or password"), 401
    else:
        # if method is not post
        return jsonify("Method is not supported"), 405

#####################################################################################################

#####################################################################################################
# Storekeeper Endpoints

# see all skis that have state "skis available"
# http://127.0.0.1:5000/get_state


@app.route('/get_state', methods=['GET'])
def skis_available():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

# checks if the user has typed the correct username and password to access endpoint
        if authenticated == 1:

            cur = mysql.connection.cursor()

# Displays all the orders that have it state as "skis available" in the order_skis table
            orders = cur.execute(
                "SELECT * FROM `order_skis` WHERE `state`= 'skis available'")

            if orders > 0:
                orders = cur.fetchall()

            cur.close()
            return jsonify(orders), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Change state to "ready to be shipped" from "skis available" for the requestet order_nr
# http://127.0.0.1:5000/change_state_ready

@app.route('/change_state_ready', methods=['POST'])
def change_state_ready():

    if request.method == 'POST':
        data = request.get_json()
        nr = data['nr']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

# checking if the order exist in the order_skis table
            order_exisist = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if order_exisist > 0:
                order_exisist = cur.fetchall()
            else:
                return jsonify("This order does not exisist"), 400

# checking if the order is in the "skis avaiable" state
            if order_exisist[0][5] != "skis available":
                return jsonify("This order is NOT in the 'skis available' state"), 400

# Change the state of the order to  "ready to be shipped"
            change_state = cur.execute(
                "UPDATE `order_skis` SET `state`='ready to be shipped' WHERE `nr`=%s AND `state`='skis available' ", (nr,))
            mysql.connection.commit()

 # displays the order that is changed
            change_info = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if change_info > 0:
                change_info = cur.fetchall()

            cur.close()
            return jsonify(change_info), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# creating a transion record for orders when more skis are assigned, we are implenting this with a comment attribute
# http://127.0.0.1:5000/storekeeper_comment

@app.route('/storekeeper_comment', methods=['POST'])
def storekeeper_comment():
    if request.method == 'POST':
        data = request.get_json()
        nr = data['nr']
        comment = data['comment']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

# checking if the order_nr selected in the json-body exist in the table, error message if not
            order_exisist = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if order_exisist == 0:
                return jsonify("This order does not exisist"), 400

# give the selected order a comment from the json-body representing its transion record
            change_comment = cur.execute(
                "UPDATE `order_skis` SET `comment`=%s WHERE `nr`=%s", (comment, nr))
            mysql.connection.commit()

# displays all the informastion for the changed order
            change_transion_record = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if change_transion_record > 0:
                change_transion_record = cur.fetchall()

            cur.close()
            return jsonify(change_transion_record), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Retrieve all the orders from the order_skis table
# http://127.0.0.1:5000/get_the_orders

@app.route('/get_the_orders', methods=['GET'])
def retrieve_orders():

    if request.method == 'GET':

        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

# displays all the orders in the order_skis table
            retrieve = cur.execute("SELECT * FROM `order_skis`")

            if retrieve > 0:
                retrieve = cur.fetchall()

            cur.close()
            return jsonify(retrieve), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Create new record for a newly produced ski:
# http://127.0.0.1:5000/storekeeper/create

@app.route('/storekeeper/create', methods=['POST'])
def create_product():

    if request.method == 'POST':
        data = request.get_json()
        id = data['id']
        model = data['model']
        type = data['type']
        size = data['size']
        photourl = data['photourl']
        description = data['description']
        historical = data['historical']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            product_exisist = cur.execute(
                "SELECT * FROM `product` WHERE `id`= %s", (id, ))

# checking if the primary key ID is taken or not, if so a error message comes up
            if product_exisist == 1:
                return jsonify("This product_nr already exisist"), 400

# create and insert the new product in the product table
            create_product = cur.execute("INSERT INTO `product`(`id`, `model`, `type`, `size`, `photourl`, `description`, `historical`) VALUES (%s, %s, %s, %s, %s, %s, %s)", (
                id, model, type, size, photourl, description, historical,))

            mysql.connection.commit()

# displays all the information for the newly created product
            product_info = cur.execute(
                "SELECT * FROM `product` WHERE `id`=%s", (id, ))

            if product_info > 0:
                product_info = cur.fetchall()

            cur.close()
            return jsonify(product_info), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405

#####################################################################################################

#####################################################################################################
# transporter endpoints

# Displays all orders that have state "ready to be shipped" :
# http://127.0.0.1:5000/get_ready_orders

@app.route('/get_ready_orders', methods=['GET'])
def get_ready_orders():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "transport")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            # gets the orders that are ready to be shipped
            orders = cur.execute(
                "SELECT * FROM `order_skis` WHERE `state`= 'ready to be shipped'")

            if orders > 0:
                orders = cur.fetchall()
            else:
                # if there are no orders that are ready to be shipped
                return jsonify("There are no orders that are ready to be shipped"), 200

            cur.close()
            return jsonify(orders), 200
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405

# Displays all shipments that have state "ready" or "picked up" while having matching driver-id:
# http://127.0.0.1:5000/get_ready_shipments

@app.route('/get_ready_shipments', methods=['GET'])
def get_ready_shipments():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "transport")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            driver_id = ''
            if request.args.get("driver_id"):
                # gets the driver_id from the url
                driver_id = request.args.get("driver_id")

            if driver_id == '':
                # if there is no driver_id in the url
                return jsonify("You need to add your driver_id to the url"), 400

            orders = cur.execute(
                "SELECT * FROM `shipment` WHERE `driver_id`=%s AND `state`= 'ready' OR `state`= 'picked up' AND `driver_id`=%s", (driver_id, driver_id,))

            if orders > 0:
                orders = cur.fetchall()
            else:
                return jsonify("There are no ready or picked up shipments for you to change"), 200

            cur.close()
            return jsonify(orders), 200
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405

# Changes the state of the shipment to "picked up", changes the comment and update the state of the orders in the 
# shipment to shipped:
# http://127.0.0.1:5000/pickup_shipment

@app.route('/pickup_shipment', methods=['POST'])
def pickup_shipment():

    if request.method == 'POST':
        comment = None
        data = request.get_json()  # gets the json-data it needs
        nr = data['nr']
        driver_id = data['driver_id']
        comment = data['comment']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "transport")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            change_shipment_state = cur.execute(
                "UPDATE `shipment` SET `state`='picked up' WHERE `nr`=%s AND `driver_id`=%s AND `state`='ready'", (nr, driver_id))
            mysql.connection.commit()

            if comment == "":  # sets comment to NULL if the user hasn't set a comment in the comment field
                change_shipment_comment = cur.execute(
                    "UPDATE `shipment` SET `comment`=%s WHERE `nr`=%s AND `driver_id`=%s", (None, nr, driver_id,))
                mysql.connection.commit()

            else:  # if the user write something in the comment field, put it in the shipment, even if the state is already set to picked up
                change_shipment_comment = cur.execute(
                    "UPDATE `shipment` SET `comment`=%s WHERE `nr`=%s AND `driver_id`=%s", (comment, nr, driver_id,))
                mysql.connection.commit()

            shipment_info = cur.execute(
                "SELECT * FROM `shipment` WHERE `nr`= %s", (nr,))

            if shipment_info > 0:
                shipment_info = cur.fetchall()
            else:
                return jsonify("There are no shipment that match this nr"), 200

            if shipment_info[0][5] == "picked up":
                # if the shipment is now picked up, sets the orders in the shipment to shipped
                change_order_state = cur.execute(
                    "UPDATE `order_skis` SET `state`='shipped' WHERE `shippment_nr`=%s", (nr,))
                mysql.connection.commit()

            if shipment_info[0][4] != driver_id:
                # if the driver_id does not match
                return jsonify("Your driver_id does not match the nr"), 400

            cur.close()
            return jsonify(shipment_info), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405

# Changes the state of a shipment to "delivered" if both id and driver-id match and updates the comment:
# http://127.0.0.1:5000/delivered_shipment

@app.route('/delivered_shipment', methods=['POST'])
def delivered_shipment():

    if request.method == 'POST':
        data = request.get_json()
        nr = data['nr']
        driver_id = data['driver_id']
        comment = data['comment']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "transport")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            change_shipment_state = cur.execute(
                "UPDATE `shipment` SET `state`='delivered' WHERE `nr`=%s AND `driver_id`=%s AND `state`='picked up'", (nr, driver_id))
            mysql.connection.commit()

            if comment == "":
                change_shipment_comment = cur.execute(
                    "UPDATE `shipment` SET `comment`=%s WHERE `nr`=%s AND `driver_id`=%s", (None, nr, driver_id,))
                mysql.connection.commit()

            else:
                change_shipment_comment = cur.execute(
                    "UPDATE `shipment` SET `comment`=%s WHERE `nr`=%s AND `driver_id`=%s", (None, nr, driver_id,))
                mysql.connection.commit()

            shipment_info = cur.execute(
                "SELECT * FROM `shipment` WHERE `nr`= %s", (nr,))

            if shipment_info > 0:
                shipment_info = cur.fetchall()
            else:
                return jsonify("There are no shipment that match this nr"), 200

            if shipment_info[0][4] != driver_id:
                return jsonify("Your driver_id does not match the nr"), 400

            cur.close()
            return jsonify(shipment_info), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405

#####################################################################################################

#####################################################################################################
# customer endpoints

# see all the production plan for 4 week back in time from the date that is given in the URL
# Example on URL http://127.0.0.1:5000/get_production_plan?from_date=2022-03-28 where date has to be in in this format "yyyy-mm-dd"


@app.route('/get_production_plan', methods=['GET'])
def production_plan():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            from_date = ''

            if request.args.get("from_date"):
                from_date = request.args.get("from_date")

            format = "%Y-%m-%d"
            res = True

# checks if the user typed the right date formate in the URL, in the form yyyy-mm-dd
            try:
                res = bool(datetime.strptime(from_date, format))
            except ValueError:
                res = False

            if(res == False):
                return jsonify("You typed the date wrong"), 400

# gets all the information for all the prodution-plan register 4 weeks before the given date
            plan = cur.execute(
                "SELECT * FROM `production_plan` WHERE start_date BETWEEN DATE_SUB(%s,INTERVAL 28 DAY) AND %s", ([from_date], [from_date]))
            mysql.connection.commit()

            if plan > 0:
                plan = cur.fetchall()

            cur.close()
            return jsonify(plan), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Delete a order based on order nr given in the URL
# Example for URL http://127.0.0.1:5000/delete_order

@app.route('/delete_order', methods=['POST'])
def delete_order():

    if request.method == 'POST':
        data = request.get_json()
        nr = data['nr']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")

        if authenticated == 1:

            cur = mysql.connection.cursor()

# Checks if the order requested exisist, if not a error message comes up
            check_if_existing = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`= %s", (nr, ))

            if check_if_existing == 0:
                return jsonify("This order does not exisist"), 400

# Delete the requested order
            order_delete = cur.execute(
                "DELETE FROM `order_skis` WHERE `nr`=%s", (nr, ))
            mysql.connection.commit()

            cur.close()
            return jsonify("This order is deleted now"), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Create a new order and if the user types incorrect input, then there will be a error message
# example on URL http://127.0.0.1:5000/create_new_order

@app.route('/create_new_order', methods=['POST'])
def new_order():

    if request.method == 'POST':
        data = request.get_json()
        nr = data['nr']
        Order_date = data['Order_date']
        franchise_id = data['franchise_id']
        individual_store_id = data['individual_store_id']
        team_skier_id = data['team_skier_id']
        state = data['state']
        comment = data['comment']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            # Check if the order nr are in use
            order_exisist = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if order_exisist == 1:
                return jsonify("This order nr is already taken"), 400

            # insuring that the user chooses only one receiver for the order, if the user chooses more than 1 recevier, than a error message comes up
            antall = 0

            if (franchise_id == None):
                antall += 1

            if (individual_store_id == None):
                antall += 1

            if (team_skier_id == None):
                antall += 1

            if(antall < 2):
                return jsonify(" ERROR, Only ONE of the following attributes must have an value: `franchise_id`,`individual_store_id`,`team_skier_id`"), 400


# Insert the new order in the order_skis table
            order_add = cur.execute("INSERT INTO `order_skis`(`nr`, `Order_date`, `franchise_id`, `individual_store_id`, `team_skier_id`, `state`, `comment`) VALUES (%s,%s,%s,%s,%s,%s,%s)", (
                nr, Order_date, franchise_id, individual_store_id, team_skier_id, state, comment,))
            mysql.connection.commit()

# show all the information for the new order
            see_orders = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if see_orders > 0:
                see_orders = cur.fetchall()

                cur.close()
                return jsonify(see_orders), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# Gets all the order since from a date that is spesfifed from the user
# http://127.0.0.1:5000/get_orders_since

@app.route('/get_orders_since', methods=['GET'])
def get_order_since():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")

        if authenticated == 1:
            if request.method == 'GET':

                cur = mysql.connection.cursor()

                if request.args.get("date"):
                    date = request.args.get("date")

                format = "%Y-%m-%d"
                res = True

# checks if the user typed the right date formate in the Json-body in the form yyyy-mm-dd, otherwise a error message comes up
                try:
                    res = bool(datetime.strptime(date, format))
                except ValueError:
                    res = False

                if(res == False):
                    return jsonify("You typed the date wrong"), 400

 # gets all the orders since that date
                since_dato = cur.execute(
                    "SELECT * FROM `order_skis` WHERE `order_date`>= %s", ([date]))

                if since_dato > 0:
                    since_dato = cur.fetchall()

                cur.close()
                return jsonify(since_dato), 201

            else:
                return jsonify("Method is not supporteds"), 405


# Gets a order with a spefic ID and gets the informasjon of the state it is in
# http://127.0.0.1:5000/get_the_order

@app.route('/get_the_order', methods=['GET'])
def get_the_order():

    if request.method == 'GET':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")

        if authenticated == 1:

            cur = mysql.connection.cursor()

            the_id = ''

# gets the id from the URL
            if request.args.get("the_id"):
                the_id = request.args.get("the_id")

# Checks if the order exisist otherwise a error message comes up
            check_if_existing = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`= %s", ([the_id]))

            if check_if_existing == 0:
                return jsonify("This order does not exisist"), 400

# gets all the informasjon for that order
            the_order = cur.execute(
                "SELECT * FROM order_skis INNER JOIN order_state ON order_skis.state = order_state.state WHERE nr=%s", ([the_id]))

            if the_order > 0:
                the_order = cur.fetchall()

            cur.close()
            return jsonify(the_order), 201
        else:
            return jsonify("Wrong username or password"), 401
    else:
        return jsonify("Method is not supported"), 405


# creating a new order for unassiged items in current order, and making the current order to status "ready to be shipped"
# http://127.0.0.1:5000/creating_unassigned_items

@app.route('/creating_unassigned_items', methods=['POST'])
def split_order():

    if request.method == 'POST':
        data = request.get_json()
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "customer")
        nr = data['nr']
        Order_date = data['Order_date']
        franchise_id = data['franchise_id']
        individual_store_id = data['individual_store_id']
        team_skier_id = data['team_skier_id']
        state = data['state']
        comment = data['comment']
        reference_order_nr = data['reference_order_nr']

        if authenticated == 1:
            cur = mysql.connection.cursor()

# checking if the reference_order_nr for the new order exist.
# because if its not exisisting you cant make the order_status to "ready to be shipped" and you cant make a new order
            exiting_order = cur.execute(
                "SELECT * FROM order_skis WHERE nr = %s", (reference_order_nr,))

            if exiting_order > 0:
                exiting_order = cur.fetchall()
            else:
                return jsonify("ERROR this reference_order_nr does not exist"), 400

# checking if the refernece_order_nr has its status not set to "ready to be shipped", and if its set to "ready to be shipped" you cant make new order
            if exiting_order[0][5] == "ready to be shipped":
                return jsonify("ERROR this refernce order is already set to 'ready to be shipped'"), 400

# insert the new order in the order_skis table
            the_new_order = cur.execute("INSERT INTO `order_skis`(`nr`, `Order_date`, `franchise_id`, `individual_store_id`, `team_skier_id`, `state`, `reference_order_nr`, `comment`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
                                        (nr, Order_date, franchise_id, individual_store_id, team_skier_id, state, reference_order_nr, comment,))

# change the order_state of the order the new order is referncing to, change it to "ready to be shipped"
            update_status = cur.execute(
                "UPDATE `order_skis` SET `state`='ready to be shipped'  WHERE `nr`=%s", (reference_order_nr, ))

            mysql.connection.commit()

# shows all the information for the new order
            see_orders = cur.execute(
                "SELECT * FROM `order_skis` WHERE `nr`=%s", (nr, ))

            if see_orders > 0:
                see_orders = cur.fetchall()

                cur.close()
                return jsonify(see_orders), 201
        else:
            return jsonify("Wrong username or password"), 401

    else:
        return jsonify("Method is not supported"), 405

#####################################################################################################

#####################################################################################################
# Prodution_plan endpoints

# creates a new prodution in the production_plan table
# http://127.0.0.1:5000/create_productionPlan


@app.route('/create_productionPlan', methods=['POST'])
def create_productionplan():

    if request.method == 'POST':
        data = request.get_json()
        id = data['id']
        start_date = data['start_date']
        username = data['username']
        password = data['password']
        authenticated = login_type(username, password, "employee")

        if authenticated == 1:
            cur = mysql.connection.cursor()

# checking if the prodution_plan id is taken or not, so that we dont get a primary key constraint error
            existing_plan = cur.execute(
                "SELECT * FROM `production_plan` WHERE `id` = %s", (id, ))

            if existing_plan > 0:
                return jsonify("ERROR this ID is already in use"), 400

            format = "%Y-%m-%d"
            res = True

# checing if the user have typed the date in the correct format in the json-body, otherwise a erro message comes up
            try:
                res = bool(datetime.strptime(start_date, format,))
            except ValueError:
                res = False

            if(res == False):
                return jsonify("You typed the date in a wrong format"), 400

# insert the new production_plan in the production_plan table
            new_plan = cur.execute(
                "INSERT INTO `production_plan`(`id`, `start_date`) VALUES (%s,%s)", (id, start_date,))
            mysql.connection.commit()

 # displays the information of new the production_plans in the prodution_plan table
            see_plans = cur.execute(
                "SELECT * FROM `production_plan` WHERE `id`=%s", (id, ))

            if see_plans > 0:
                see_plans = cur.fetchall()

                cur.close()
                return jsonify(see_plans), 201

        else:
            return jsonify("Wrong username or password"), 401

    else:
        return jsonify("Method is not supported"), 405

#####################################################################################################

#####################################################################################################
# login function

# login function that takes username, unhashed password and customer table as parameters,
# it then looks up the username in the given table and hashes the password using the salt.
# finally it compares the newly hashed password with the hashed password in the database table


def login_type(username, password, table):

    logged_in = 0

    cur = mysql.connection.cursor()

    # reads hashed password and salt from the correct table based on parameter
    if table == "customer":
        get_salt = cur.execute(
            "SELECT `password`, `salt` FROM `customer_login` WHERE `username` = %s", (username,))
    elif table == "employee":
        get_salt = cur.execute(
            "SELECT `password`, `salt` FROM `employee_login` WHERE `username` = %s", (username,))
    elif table == "transport":
        get_salt = cur.execute(
            "SELECT `password`, `salt` FROM `transport_login` WHERE `username` = %s", (username,))
    else:
        return 0

    if get_salt > 0:
        get_salt = cur.fetchall()
    else:
        return 0

    # adds the salt to the input password
    list_password = list(password)
    list_password.insert(2, get_salt[0][1])
    salted_password = ''.join([str(item) for item in list_password])

    # encodes password so it is ready for hashing
    encoded_password = salted_password.encode()

    # hashes the password
    hash_object = hashlib.sha256(encoded_password)

    # hexdigests the hashed password to make it human readable
    hashed_password = hash_object.hexdigest()

    # compare newly hashed password with hashed password from the database
    if hashed_password == get_salt[0][0]:
        logged_in = 1

    return logged_in


if __name__ == '__main__':
    app.run(debug=True)
