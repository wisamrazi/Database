from pickle import NONE
import unittest
import requests


class Test_Public(unittest.TestCase):
    GET_PUBLIC_WITHOUT_API_URL = "http://127.0.0.1:5000//get_product_type"
    GET_PUBLIC_WITHSOME_API_URL = "http://127.0.0.1:5000//get_product_type?model=Active&min_size=152"
    GET_PUBLIC_WITHALL_API_URL = "http://127.0.0.1:5000//get_product_type?model=Active&min_size=152&max_size=172"
    GET_PUBLIC_API_URL_NORESULT = "http://127.0.0.1:5000//get_product_type?min_size=152&max_size=147"

    def test_1_check_get_public_api_nofilters(self):

        response = requests.get(Test_Public.GET_PUBLIC_WITHOUT_API_URL)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_2_check_get_public_api_somefilters(self):

        response = requests.get(Test_Public.GET_PUBLIC_WITHSOME_API_URL)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_3_check_get_public_api_allfilters(self):

        response = requests.get(Test_Public.GET_PUBLIC_WITHALL_API_URL)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_4_check_get_public_api_allfilters(self):

        response = requests.get(Test_Public.GET_PUBLIC_API_URL_NORESULT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), 0)


class Test_Transporter(unittest.TestCase):

    GET_TRANSPORTER_ORDERS_API_URL = "http://127.0.0.1:5000/get_ready_orders"
    GET_TRANSPORTER_SHIPMENTS_API_URL_correct = "http://127.0.0.1:5000/get_ready_shipments?driver_id=1"
    GET_TRANSPORTER_SHIPMENTS_API_URL_incorrect = "http://127.0.0.1:5000/get_ready_shipments"
    POST_TRANSPORTER_PICKUP_API_URL = "http://127.0.0.1:5000/pickup_shipment"
    POST_TRANSPORTER_DELIVERED_API_URL = "http://127.0.0.1:5000//delivered_shipment"

    GET_TRANSPORTER_JSON_CORRECT_INPUT = {
        "username": "Bring1",
        "password": "password"
    }

    GET_TRANSPORTER_JSON_INCORRECT_INPUT = {
        "username": "Bring1",
        "password": "feilpassword"
    }

    POST_TRANSPORTER_PICKUP_JSON_CORRECT_INPUT = {
        "username": "Bring1",
        "password": "password",
        "nr": 1,
        "driver_id": 1,
        "comment": ""
    }

    POST_TRANSPORTER_PICKUPDELIVER_JSON_INCORRECT_INPUT = {
        "username": "Bring1",
        "password": "password",
        "nr": 1,
        "driver_id": 4,
        "comment": ""
    }

    POST_TRANSPORTER_DELIVERED_JSON_CORRECT_INPUT = {
        "username": "Bring1",
        "password": "password",
        "nr": 2,
        "driver_id": 2,
        "comment": ""
    }

    POST_TRANSPORTER_PICKUP_JSON_CORRECT_OUTPUT = [[
        1,
        "Intersport",
        "Hammersborggate 9, 0181 Oslo",
        "Thu, 21 Apr 2022 00:00:00 GMT",
        1,
        "picked up",
        "Bring",
        None,
        None,
        1,
        None
    ]]

    POST_TRANSPORTER_DELIVERED_JSON_CORRECT_OUTPUT = [[
        2,
        "XXL Lysaker",
        "Emanuels vei 12, 0283 Oslo\r\n",
        "Wed, 23 Mar 2022 00:00:00 GMT",
        2,
        "delivered",
        "Posten",
        None,
        2,
        None,
        None
    ]]

    def test_1_check_get_transporter_orders_api(self):

        response = requests.get(Test_Transporter.GET_TRANSPORTER_ORDERS_API_URL,
                                json=Test_Transporter.GET_TRANSPORTER_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_2_check_get_transporter_shipments_api_correct(self):

        response = requests.get(Test_Transporter.GET_TRANSPORTER_SHIPMENTS_API_URL_correct,
                                json=Test_Transporter.GET_TRANSPORTER_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_3_check_get_transporter_shipments_api_incorrect_json(self):

        response = requests.get(Test_Transporter.GET_TRANSPORTER_SHIPMENTS_API_URL_correct,
                                json=Test_Transporter.GET_TRANSPORTER_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 401)

    def test_4_check_get_transporter_shipments_api_incorrect_url(self):

        response = requests.get(Test_Transporter.GET_TRANSPORTER_SHIPMENTS_API_URL_incorrect,
                                json=Test_Transporter.GET_TRANSPORTER_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

    def test_5_check_post_transporter_pickup_correct_input(self):

        response = requests.post(Test_Transporter.POST_TRANSPORTER_PICKUP_API_URL,
                                 json=Test_Transporter.POST_TRANSPORTER_PICKUP_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Transporter.POST_TRANSPORTER_PICKUP_JSON_CORRECT_OUTPUT)

    def test_6_check_post_transporter_pickup_incorrect_driver_id(self):

        response = requests.post(Test_Transporter.POST_TRANSPORTER_PICKUP_API_URL,
                                 json=Test_Transporter.POST_TRANSPORTER_PICKUPDELIVER_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

    def test_7_check_post_transporter_delivered_correct_input(self):

        response = requests.post(Test_Transporter.POST_TRANSPORTER_DELIVERED_API_URL,
                                 json=Test_Transporter.POST_TRANSPORTER_DELIVERED_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Transporter.POST_TRANSPORTER_DELIVERED_JSON_CORRECT_OUTPUT)

    def test_8_check_post_transporter_delivered_incorrect_driver_id(self):

        response = requests.post(Test_Transporter.POST_TRANSPORTER_DELIVERED_API_URL,
                                 json=Test_Transporter.POST_TRANSPORTER_PICKUPDELIVER_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


class Test_Customer_rep(unittest.TestCase):

    GET_CUSTOMER_REP_ORDERS_API_URL = "http://127.0.0.1:5000/customer/rep/retrieve"
    GET_CUSTOMER_REP_ORDERS_WITHFILTER_API_URL = "http://127.0.0.1:5000/customer/rep/retrieve?status=new"
    POST_CUSTOMER_REP_CHANGESTATE_API_URL = "http://127.0.0.1:5000/customer/rep/changeState"

    GET_CUSTOMER_REP_ORDER_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword"
    }

    GET_CUSTOMER_REP_ORDER_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "feilpassword"
    }

    POST_CUSTOMER_REP_CHANGESTATE_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 6
    }

    POST_CUSTOMER_REP_CHANGESTATE_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 5426
    }

    POST_CUSTOMER_REP_CHANGESTATE_JSON_CORRECT_OUTPUT = [[
        6,
        "Mon, 07 Mar 2022 00:00:00 GMT",
        None,
        None,
        1,
        "open",
        None,
        None,
        None
    ]
    ]

    def test_1_check_get_customer_rep_orders_api(self):

        response = requests.get(Test_Customer_rep.GET_CUSTOMER_REP_ORDERS_API_URL,
                                json=Test_Customer_rep.GET_CUSTOMER_REP_ORDER_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_2_check_get_customer_rep_orders_withfilter_api(self):

        response = requests.get(Test_Customer_rep.GET_CUSTOMER_REP_ORDERS_WITHFILTER_API_URL,
                                json=Test_Customer_rep.GET_CUSTOMER_REP_ORDER_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 200)

    def test_3_check_get_customer_rep_orders_incorrectpassword_api(self):

        response = requests.get(Test_Customer_rep.GET_CUSTOMER_REP_ORDERS_API_URL,
                                json=Test_Customer_rep.GET_CUSTOMER_REP_ORDER_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 401)

    def test_4_check_post_customer_rep_changestate_api(self):

        response = requests.post(Test_Customer_rep.POST_CUSTOMER_REP_CHANGESTATE_API_URL,
                                 json=Test_Customer_rep.POST_CUSTOMER_REP_CHANGESTATE_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(
        ), Test_Customer_rep.POST_CUSTOMER_REP_CHANGESTATE_JSON_CORRECT_OUTPUT)

    def test_5_check_post_customer_rep_changestate_incorrect_api(self):

        response = requests.post(Test_Customer_rep.POST_CUSTOMER_REP_CHANGESTATE_API_URL,
                                 json=Test_Customer_rep.POST_CUSTOMER_REP_CHANGESTATE_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# Testing for the storekeeper endpoints


class Test_Storekeeper(unittest.TestCase):

    GET_STOREKEEPER_STATE_API_URL = "http://127.0.0.1:5000/get_state"
    GET_STOREKEEPER_ORDER_API_URL = "http://127.0.0.1:5000/get_the_orders"
    POST_STOREKEEPER_CHANGESTATE_API_URL = "http://127.0.0.1:5000/change_state_ready"
    POST_STOREKEEPER_COMMENT_API_URL = "http://127.0.0.1:5000/storekeeper_comment"
    POST_STOREKEEPER_CREATE_API_URL = "http://127.0.0.1:5000/storekeeper/create"

    GET_STOREKEEPER_STATE_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword"
    }

    GET_STOREKEEPER_STATE_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "feilpassword"
    }

    POST_STOREKEEPER_CHANGESTATE_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 5
    }

    POST_STOREKEEPER_CHANGESTATE_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 2341
    }

    POST_STOREKEEPER_COMMENT_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 2,
        "comment": "This order now has a comment"
    }

    POST_STOREKEEPER_COMMENT_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "nr": 1253,
        "comment": "This order now has a comment"
    }

    POST_STOREKEEPER_CREATE_JSON_CORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "id": 100,
        "model": "active",
        "type": "skate",
        "size": 152,
        "photourl": "google.com/image",
        "description": "blabal",
        "historical": "1"
    }

    POST_STOREKEEPER_CREATE_JSON_INCORRECT_INPUT = {
        "username": "Andreas1",
        "password": "ikkepassword",
        "id": 1,
        "model": "active",
        "type": "skate",
        "size": 152,
        "photourl": "google.com/image",
        "description": "blabal",
        "historical": 1
    }

    POST_STOREKEEPER_CHANGESTATE_JSON_CORRECT_OUTPUT = [[
        5,
        "Wed, 16 Feb 2022 00:00:00 GMT",
        2,
        None,
        None,
        "ready to be shipped",
        None,
        None,
        None
    ]]

    POST_STOREKEEPER_COMMENT_JSON_CORRECT_OUTPUT = [[
        2,
        "Tue, 11 Jan 2022 00:00:00 GMT",
        1,
        None,
        None,
        "ready to be shipped",
        None,
        None,
        "This order now has a comment"
    ]]

    POST_STOREKEEPER_CREATE_JSON_CORRECT_OUTPUT = [[
        100,
        "active",
        "skate",
        152,
        "google.com/image",
        "blabal",
        1,
        0
    ]]

# Tests if the  endpoint returns all orders that is in the "skis avalible" with the correct input
    def test_1_check_get_storekeeper_state_api(self):

        response = requests.get(Test_Storekeeper.GET_STOREKEEPER_STATE_API_URL,
                                json=Test_Storekeeper.GET_STOREKEEPER_STATE_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)

# Tests if the user gets denined access if the incorrect login credentials are typed in the json body
    def test_2_check_get_storekeeper_state_incorrect_api(self):

        response = requests.get(Test_Storekeeper.GET_STOREKEEPER_STATE_API_URL,
                                json=Test_Storekeeper.GET_STOREKEEPER_STATE_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 401)

# Tests if the all the orders from order_skis table are retrived
    def test_3_check_get_storekeeper_order_api(self):
        response = requests.get(Test_Storekeeper.GET_STOREKEEPER_ORDER_API_URL,
                                json=Test_Storekeeper.GET_STOREKEEPER_STATE_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)

# Tests if the endpoint changes the state of an existing order to "ready to be shipped" from "skis available", if the selected order is in the "skis avalible state"
# and the endpoint returns the correct output
    def test_4_check_get_storekeeper_changestate_correct_api(self):
        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_CHANGESTATE_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_CHANGESTATE_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json(
        ), Test_Storekeeper.POST_STOREKEEPER_CHANGESTATE_JSON_CORRECT_OUTPUT)

# Tests if the endpoint does not accept the change of state if the order does not exisist
    def test_5_check_get_storekeeper_changestate_incorrect_api(self):
        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_CHANGESTATE_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_CHANGESTATE_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# Tests if the endpoint adds a comment from the correct json-body to the requestetd order and returnes the correct output
    def test_6_check_get_storekeeper_comment_correct_api(self):

        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_COMMENT_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_COMMENT_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Storekeeper.POST_STOREKEEPER_COMMENT_JSON_CORRECT_OUTPUT)

# tests if the endpoint does not accept a not exisiting order that were given in the incorrect json-body
    def test_7_check_get_storekeeper_comment_incorrect_api(self):

        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_COMMENT_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_COMMENT_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# Tests if the endpoint create a new product based on the correct json-body given, and the endpoint provide the correct output
    def test_8_check_get_storekeeper_create_correct_api(self):

        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_CREATE_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_CREATE_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Storekeeper.POST_STOREKEEPER_CREATE_JSON_CORRECT_OUTPUT)

# Test if the endpoint does not create a new product that has a duplicate primary key that is given in the incorrect json-body
    def test_9_check_get_storekeeper_create_incorrect_api(self):

        response = requests.post(Test_Storekeeper.POST_STOREKEEPER_CREATE_API_URL,
                                 json=Test_Storekeeper.POST_STOREKEEPER_CREATE_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


# testing for creating a new production plan for the production planner endpoint
class Test_Production_plan(unittest.TestCase):
    POST_PLAN_URL = "http://127.0.0.1:5000/create_productionPlan"
    CREATE_PLAN_TEST_JSON_CORRECT_INPUT = {
        "username": "Espen1",
        "password": "password",
        "id": 555,
        "start_date": "2022-12-11"
    }

    CREATE_PLAN_TEST_JSON_INCORRECT_INPUT = {
        "username": "Espen1",
        "password": "password",
        "id": 1,
        "start_date": "2022-12-11"
    }

    CREATE_PLAN_TEST_JSON_INCORRECT_LOGIN = {
        "username": "Espen1",
        "password": "drowssap",
        "id": 401,
        "start_date": "2022-12-11"
    }

    CREATE_PLAN_TEST_JSON_CORRECT_OUTPUT = [[
        555,
        "Sun, 11 Dec 2022 00:00:00 GMT"
    ]]

# tests if the endpoint works with correct input and gives the right output
    def test_1_check_post_correct_input_plan(self):
        response = requests.post(Test_Production_plan.POST_PLAN_URL,
                                 json=Test_Production_plan.CREATE_PLAN_TEST_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Production_plan.CREATE_PLAN_TEST_JSON_CORRECT_OUTPUT)

# tests if the endpoint gives doesnt accepts duplicate primary key
    def test_2_check_post_incorrect_input_plan(self):
        response = requests.post(Test_Production_plan.POST_PLAN_URL,
                                 json=Test_Production_plan.CREATE_PLAN_TEST_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# tests if the user gets uautorized access when insert the wrong login information
    def test_3_check_post_incorrect_Login_plan(self):
        response = requests.post(Test_Production_plan.POST_PLAN_URL,
                                 json=Test_Production_plan.CREATE_PLAN_TEST_JSON_INCORRECT_LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 401)


# testing for the deleting order for the customer endpoint
class Test_Customer_delete(unittest.TestCase):

    GET_CUSTOMER_DELETE_URL = "http://127.0.0.1:5000/delete_order"

    CUSTOMER_DELETE_TEST_JSON_CORRECT_INPUT = {
        "username": "XXL1",
        "password": "password",
        "nr": 1
    }

    CUSTOMER_DELETE_TEST_JSON_INCORRECT_INPUT = {
        "username": "XXL1",
        "password": "password",
        "nr": 10000,
    }

    CUSTOMER_DELETE_TEST_JSON_WRONG_LOGIN = {
        "username": "XXL1",
        "password": "drowssap",
        "nr": 5,
    }

    CUSTOMER_DELETE_TEST_JSON_CORRECT_OUTPUT = 'This order is deleted now'

# tests if the order is deleted correctly and return the correct output
    def test_1_check_delete_customer_corrcet_input(self):
        response = requests.post(Test_Customer_delete.GET_CUSTOMER_DELETE_URL,
                                 json=Test_Customer_delete.CUSTOMER_DELETE_TEST_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Customer_delete.CUSTOMER_DELETE_TEST_JSON_CORRECT_OUTPUT)

# tests if endpoint doesnt accept a value of a order that doesnt exist
    def test_2_check_delete_incorrect_input(self):
        response = requests.post(Test_Customer_delete.GET_CUSTOMER_DELETE_URL,
                                 json=Test_Customer_delete.CUSTOMER_DELETE_TEST_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# tests if the user gets uautorized access when insert the wrong login information
    def test_3_check_login(self):
        response = requests.post(Test_Customer_delete.GET_CUSTOMER_DELETE_URL,
                                 json=Test_Customer_delete.CUSTOMER_DELETE_TEST_JSON_WRONG_LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 401)


# testing for creating a new order for the customer endpoint
class Test_Customer_New_Order(unittest.TestCase):

    GET_CUSTOMER_NEW_ORDER_URL = "http://127.0.0.1:5000/create_new_order"

    CUSTOMER_NEW_ORDER_TEST_JSON_CORRECT_INPUT = {
        "username": "XXL1",
        "password": "password",
        "nr": 200,
        "Order_date": "2022-11-21",
        "franchise_id": 1,
        "individual_store_id": None,
        "team_skier_id": None,
        "state": "new",
        "comment": "blablabla"
    }

    CUSTOMER_NEW_ORDER_TEST_JSON_INCORRECT_INPUT = {
        "username": "XXL1",
        "password": "password",
        "nr": 1,
        "Order_date": "2022-11-21",
        "franchise_id": 2,
        "individual_store_id": 1,
        "team_skier_id": 1,
        "state": "new",
        "comment": "blablabla"
    }

    CUSTOMER_NEW_ORDER_TEST_JSON_CORRECT_OUTPUT = [[
        200,
        "Mon, 21 Nov 2022 00:00:00 GMT",
        1,
        None,
        None,
        "new",
        None,
        None,
        "blablabla"
    ]]

# checks if the order is created successfuly and it returns the correct output
    def test_1_check_customer_New_order_corrcet_input(self):
        response = requests.post(
            Test_Customer_New_Order.GET_CUSTOMER_NEW_ORDER_URL, json=Test_Customer_New_Order.CUSTOMER_NEW_ORDER_TEST_JSON_CORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Customer_New_Order.CUSTOMER_NEW_ORDER_TEST_JSON_CORRECT_OUTPUT)

# checks if the endpoint doesnt accept duplicated primary key values
    def test_2_check_customer_New_order_incorrcet_input(self):
        response = requests.post(
            Test_Customer_New_Order.GET_CUSTOMER_NEW_ORDER_URL, json=Test_Customer_New_Order.CUSTOMER_NEW_ORDER_TEST_JSON_INCORRECT_INPUT)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


# testing to get the production plan for 4 week back in time for the customer endpoint
class Test_Customer_Get_Plan_Four_Week(unittest.TestCase):

    GET_PLAN_URL_CORRECT = "http://127.0.0.1:5000/get_production_plan?from_date=2022-03-28"
    GET_PLAN_URL_INCORRECT_1 = "http://127.0.0.1:5000/get_production_plan?from_date=2022.03.28"
    GET_PLAN_URL_INCORRECT_2 = "http://127.0.0.1:5000/get_production_plan?from_date=2022-15-15"

    LOGIN = {
        "username": "XXL1",
        "password": "password",
    }

# checks for that the date is written corrcet in the URL and returns all the production plans
    def test_1_check_customer_Production_plan_Four_week_Correct(self):
        response = requests.get(
            Test_Customer_Get_Plan_Four_Week.GET_PLAN_URL_CORRECT, json=Test_Customer_Get_Plan_Four_Week.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)

# checks if the endpoint doesnt accept dates that is written in the wrong format, which means not "yyyy-mm-dd"
    def test_2_check_customer_Production_plan_Since_Incorrect_1(self):
        response = requests.get(
            Test_Customer_Get_Plan_Four_Week.GET_PLAN_URL_INCORRECT_1, json=Test_Customer_Get_Plan_Four_Week.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# checks if the date written is a valid date
    def test_3_check_customer_Production_plan_Incorrect_2(self):
        response = requests.get(
            Test_Customer_Get_Plan_Four_Week.GET_PLAN_URL_INCORRECT_2, json=Test_Customer_Get_Plan_Four_Week.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


# testing to get all the orders since a specified date in the URL, for the customer endpoint
class Test_Customer_Get_Orders_SINCE(unittest.TestCase):

    GET_ORDERS_URL_CORRECT = "http://127.0.0.1:5000/get_orders_since?date=2022-01-01"
    GET_ORDERS_URL_INCORRECT = "http://127.0.0.1:5000/get_orders_since?date=2022.01.01"
    GET_ORDERS_URL_INCORRECT_2 = "http://127.0.0.1:5000/get_orders_since?date=2022-20-20"

    LOGIN = {
        "username": "XXL1",
        "password": "password",
    }

# testing for when the date is written in the correct format in the URL and it returns the requeted orders
    def test_1_check_customer_Orders_since_correct(self):
        response = requests.get(
            Test_Customer_Get_Orders_SINCE.GET_ORDERS_URL_CORRECT, json=Test_Customer_Get_Orders_SINCE.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)

# testing for that the endpoint doesnt accept dates that is written int wrong format, which means not "yyyy-mm-dd"
    def test_2_check_customer_Orders_since_incorrect(self):
        response = requests.get(
            Test_Customer_Get_Orders_SINCE.GET_ORDERS_URL_INCORRECT, json=Test_Customer_Get_Orders_SINCE.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# checks that the user gets an error if the write a invalid date
    def test_3_check_customer_Orders_since_incorrect(self):
        response = requests.get(
            Test_Customer_Get_Orders_SINCE.GET_ORDERS_URL_INCORRECT_2, json=Test_Customer_Get_Orders_SINCE.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


# testing to get one particular order that is specified in the URL, for the customer endpoint
class Test_Customer_Get_One_Order(unittest.TestCase):

    GET_THE_ORDERS_URL_CORRECT = "http://127.0.0.1:5000/get_the_order?the_id=4"
    GET_THE_ORDERS_URL_INCORRECT = "http://127.0.0.1:5000/get_the_order?the_id=100000000"
    GET_THE_ORDERS_URL_INCORRECT_2 = "http://127.0.0.1:5000/get_the_order?the_id=ABC"

    LOGIN = {
        "username": "XXL1",
        "password": "password",
    }

# checks if the user has typed a existing order in the URL and returns a value
    def test_1_check_customer_One_Order_correct(self):
        response = requests.get(
            Test_Customer_Get_One_Order.GET_THE_ORDERS_URL_CORRECT, json=Test_Customer_Get_One_Order.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)

# checks if the endpoint doesnt accept ids for order that doesnt exists
    def test_2_check_customer_One_Order_correct(self):
        response = requests.get(
            Test_Customer_Get_One_Order.GET_THE_ORDERS_URL_INCORRECT, json=Test_Customer_Get_One_Order.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

#  checks if the endpoint doesnt accept ids for order that doesnt exists that is not numerical
    def test_3_check_customer_One_Order_correct(self):
        response = requests.get(
            Test_Customer_Get_One_Order.GET_THE_ORDERS_URL_INCORRECT_2, json=Test_Customer_Get_One_Order.LOGIN)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)


# testing for creating a new order and changing the state of the order the new order is refercenning to, to the state of "ready to be shipped" for the customer endpoint
class Test_Customer_Creating_Unassigned(unittest.TestCase):

    GET_UNASSIGED_URL = "http://127.0.0.1:5000/creating_unassigned_items"

    CORRECT_JSON_BODY = {
        "username": "XXL1",
        "password": "password",
        "nr": 10,
        "Order_date": "2022-11-12",
        "franchise_id": 2,
        "individual_store_id": None,
        "team_skier_id": None,
        "state": "new",
        "comment": "blablabak",
        "reference_order_nr": 2,
    }

    INCORRECT_JSON_BODY = {
        "username": "XXL1",
        "password": "password",
        "nr": 11,
        "Order_date": "2022-11-12",
        "franchise_id": 2,
        "individual_store_id": None,
        "team_skier_id": None,
        "state": "new",
        "comment": "blablabak",
        "reference_order_nr": 1000,
    }

    INCORRECT_JSON_BODY_2 = {
        "username": "XXL1",
        "password": "password",
        "nr": 12,
        "Order_date": "2022-11-12",
        "franchise_id": 2,
        "individual_store_id": None,
        "team_skier_id": 1,
        "state": "new",
        "comment": "blablabak",
        "reference_order_nr": 2,
    }

    CORRECT_JSON_OUTPUT = [[
        10,
        "Sat, 12 Nov 2022 00:00:00 GMT",
        2,
        None,
        None,
        "new",
        2,
        None,
        "blablabak"
    ]]

# checks if order is created succsesfuly and it has changed the refernce order to "ready to be shipped", it also checks if the correct output is provided
    def test_1_check_customer_Create__new_Unassigned_order_corrcet_input(self):
        response = requests.post(Test_Customer_Creating_Unassigned.GET_UNASSIGED_URL,
                                 json=Test_Customer_Creating_Unassigned.CORRECT_JSON_BODY)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.json(), Test_Customer_Creating_Unassigned.CORRECT_JSON_OUTPUT)

# checks if the endpoint does not create a new order if the new order has a duplicate primary key
    def test_2_check_customer_Create__new_Unassigned_order_Incorrcet_input(self):
        response = requests.post(Test_Customer_Creating_Unassigned.GET_UNASSIGED_URL,
                                 json=Test_Customer_Creating_Unassigned.INCORRECT_JSON_BODY)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)

# checks if the new order doesnt get created because the refernce order is already set to "ready to be shipped"
    def test_3_check_customer_Create__new_Unassigned_order_Incorrcet_2_input(self):
        response = requests.post(Test_Customer_Creating_Unassigned.GET_UNASSIGED_URL,
                                 json=Test_Customer_Creating_Unassigned.INCORRECT_JSON_BODY_2)
        self.assertEqual(response.headers["Content-Type"], "application/json")
        self.assertEqual(response.status_code, 400)
