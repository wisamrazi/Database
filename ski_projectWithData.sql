-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29. Mar, 2022 16:30 PM
-- Tjener-versjon: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ski_project`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `customer_login`
--

CREATE TABLE `customer_login` (
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `salt` varchar(10) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `customer_login`
--

INSERT INTO `customer_login` (`username`, `password`, `salt`) VALUES
('Intersport1', '0dadf9ad352671799bede88209ba57094ebbebadcd35cf419eaec8d4cac9e505', 'yeB5e'),
('Krogstadsport1', '65ec8bc20e72628654eac12265e772d11fb238bb5f58ecbe1adc2a03e16d1dcf', 'Kb4Es'),
('Lysakersport1', '40a0b80d0a608221f868c00c1d811019c2d19741e5a6f2be672cd6f298a23d48', '8ueF1'),
('MaritBjorgen1', '966f18e32e484f8d9240a20daae876a363780c1cd7ca35d3525ca902f80688fd', 'lk73W'),
('PetterNortug1', '76efb2a1caeae81e01fd2237f68c72af7026bf298e3f5cb65481b145a0515622', 'cGet3'),
('XXL1', '48295dff0a9486ce750befbf1316403ea0847b571e66fc0723df591d7033330b', 'dr63N');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `customer_rep`
--

CREATE TABLE `customer_rep` (
  `nr` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `customer_rep`
--

INSERT INTO `customer_rep` (`nr`, `name`, `username`) VALUES
(1, 'Andreas Johansen', 'Andreas1'),
(2, 'Espen Olsen', 'Espen1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `employee_login`
--

CREATE TABLE `employee_login` (
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `salt` varchar(10) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `employee_login`
--

INSERT INTO `employee_login` (`username`, `password`, `salt`) VALUES
('Andreas1', '53102dc5d8a17a4f355bd7e3ed7d485ac21b1e97d8f0b246b11f438eb85ffe4e', 'oO87e'),
('Espen1', 'f72f231a1ad8d3260dde4879d82f816edf044407195d827b85372a921d2d2c4c', 'ge53L'),
('Johanna1', '7c6ffd18cffd288a21a5620677d2e92919993fc8e84f37f504a9aa911806adb1', 'Swq45'),
('Lars1', 'eb2d88d5d6a183bd3a0a8eae07ed3268173395b759686147dab65d38b53ba2ca', 'h6oVb'),
('Martin1', '9c9ce3c720c5e68206e72881181fe6ad95cf6f6948974e507f64708b13bb83e6', 'ty4V2'),
('Wisam1', '9d5a966c9f0635e2cae567a67323c2ca7d9dca0b030746d6a9c0092967fbe21c', 'tR3sx');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `franchise`
--

CREATE TABLE `franchise` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `shipping_address` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `buying_price` int(11) NOT NULL,
  `store_info` varchar(2500) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `franchise`
--

INSERT INTO `franchise` (`id`, `name`, `shipping_address`, `start_date`, `end_date`, `buying_price`, `store_info`, `username`) VALUES
(1, 'Intersport', 'Hammersborggate 9, 0181 Oslo', '2022-03-22', '2023-03-22', 35, NULL, 'Intersport1'),
(2, 'XXL', 'Storgata 2, 0155 Oslo', '2022-01-09', '2023-01-09', 40, NULL, 'XXL1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `individual_store`
--

CREATE TABLE `individual_store` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `shipping_address` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `buying_price` int(11) NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `individual_store`
--

INSERT INTO `individual_store` (`id`, `name`, `shipping_address`, `start_date`, `end_date`, `buying_price`, `franchise_id`, `username`) VALUES
(1, 'Krogstad Sport', 'Krogstadveien 61, 1408 Ski', '2022-02-14', '2023-02-14', 50, NULL, 'Krogstadsport1'),
(2, 'XXL Lysaker', 'Emanuels vei 12, 0283 Oslo', '2021-05-11', '2022-05-11', 40, 2, 'Lysakersport1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `order_has`
--

CREATE TABLE `order_has` (
  `product_id` int(11) NOT NULL,
  `order_nr` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `order_has`
--

INSERT INTO `order_has` (`product_id`, `order_nr`, `quantity`) VALUES
(1, 1, 30),
(2, 4, 10),
(2, 6, 3),
(3, 1, 20),
(3, 2, 50),
(3, 5, 70),
(6, 3, 3);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `order_skis`
--

CREATE TABLE `order_skis` (
  `nr` int(11) NOT NULL,
  `Order_date` date NOT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `individual_store_id` int(11) DEFAULT NULL,
  `team_skier_id` int(11) DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `reference_order_nr` int(11) DEFAULT NULL,
  `shippment_nr` int(11) DEFAULT NULL,
  `comment` varchar(2500) COLLATE utf8mb4_danish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `order_skis`
--

INSERT INTO `order_skis` (`nr`, `Order_date`, `franchise_id`, `individual_store_id`, `team_skier_id`, `state`, `reference_order_nr`, `shippment_nr`, `comment`) VALUES
(1, '2022-02-13', 2, NULL, NULL, 'new', NULL, NULL, NULL),
(2, '2022-01-11', 1, NULL, NULL, 'new', NULL, NULL, NULL),
(3, '2021-10-03', NULL, NULL, 2, 'ready to be shipped', NULL, 3, NULL),
(4, '2021-07-11', NULL, 2, NULL, 'open', NULL, NULL, NULL),
(5, '2022-02-16', 2, NULL, NULL, 'skis available', NULL, NULL, NULL),
(6, '2022-03-07', NULL, NULL, 1, 'new', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `order_state`
--

CREATE TABLE `order_state` (
  `state` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `description` varchar(2500) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `order_state`
--

INSERT INTO `order_state` (`state`, `description`) VALUES
('new', 'open when the customer representatives have reviewed an incoming order and verified that it seems correct and complete'),
('open', 'skis available when the order is scheduled for having skies being picked'),
('ready to be shipped', 'shipped when the transporter has picked up the boxes containing the skis in the order'),
('shipped', 'order is done and has been shipped'),
('skis available', 'ready to be shipped when the storekeeper has selected and packed all the skis being ordered');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `size` int(11) NOT NULL,
  `photourl` varchar(1000) COLLATE utf8mb4_danish_ci NOT NULL,
  `description` varchar(2500) COLLATE utf8mb4_danish_ci NOT NULL,
  `historical` tinyint(1) NOT NULL,
  `msrp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product`
--

INSERT INTO `product` (`id`, `model`, `type`, `size`, `photourl`, `description`, `historical`, `msrp`) VALUES
(1, 'Active Pro', 'skate', 167, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fsportnorge.no%2Fproduct%2F100542&psig=AOvVaw3M-n6Yhjw6fwYgCFVnso9G&ust=1648200050745000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCJDRhue13vYCFQAAAAAdAAAAABAL', 'ski designet for erfarene skøyteløpere', 0, 3200),
(2, 'Endurance', 'skate', 162, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fmadshus.com%2Fen-us%2Fp%2Fendurace-skate&psig=AOvVaw2ApFrcPvYdwumfDlINKrUV&ust=1648200223218000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCNi7s7m23vYCFQAAAAAdAAAAABAF', 'ski designet for skøyteløpere som foretrekker lange turer', 0, 2500),
(3, 'Redline', 'double pole', 177, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fwww.skistart.no%2Fp-60947-madshus-redline-classic-zero-langrennski.aspx&psig=AOvVaw1HOpYme1Y2kX4BDgjYserI&ust=1648200300254000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCOiU69223vYCFQAAAAAdAAAAABAE', 'ski designet for å gå klassisk raskt', 0, 3800),
(4, 'Active', 'skate', 152, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fwww.obs.no%2Fmadshus%2F2501877&psig=AOvVaw3GwT44H4mqoW7Vj0jbf8_U&ust=1648200427064000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCLiWiZu33vYCFQAAAAAdAAAAABAD', 'active ski laget primært for junior', 1, 1200),
(5, 'Intrasonic', 'classic', 152, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fwww.sportshopen.com%2Fno-no%2Farticle%2Fintrasonic_A86190&psig=AOvVaw353vdBWnmvly7z1incfIEb&ust=1648200570535000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCJi05-S33vYCFQAAAAAdAAAAABAX', 'ski designet for klassisk langrensgåing med stilfult design', 1, 8900),
(6, 'Race Pro', 'classic', 162, 'https://www.google.no/url?sa=i&url=https%3A%2F%2Fwww.xxl.no%2Fmadshus-race-pro-classic-20-21-klassiskski-unisex%2Fp%2F1167056_1_style&psig=AOvVaw2dzPIh7l_aHLGe_nXxf9WD&ust=1648200702474000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCKDDvp643vYCFQAAAAAdAAAAABAJ', 'ski designet for å klassisk spurt', 0, 7600);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `production_has`
--

CREATE TABLE `production_has` (
  `plan_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `production_has`
--

INSERT INTO `production_has` (`plan_id`, `product_id`, `quantity`) VALUES
(1, 1, 10),
(1, 2, 10),
(1, 3, 20),
(2, 1, 15),
(2, 2, 15),
(2, 6, 10),
(3, 1, 20),
(3, 3, 20);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `production_plan`
--

CREATE TABLE `production_plan` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `production_plan`
--

INSERT INTO `production_plan` (`id`, `start_date`) VALUES
(1, '2022-03-07'),
(2, '2022-04-04'),
(3, '2022-05-02');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product_manager`
--

CREATE TABLE `product_manager` (
  `nr` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product_manager`
--

INSERT INTO `product_manager` (`nr`, `name`, `username`) VALUES
(1, 'Martin Andersen', 'Martin1'),
(2, 'Wisam Pettersen', 'Wisam1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product_model`
--

CREATE TABLE `product_model` (
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product_model`
--

INSERT INTO `product_model` (`name`) VALUES
('Active'),
('Active Pro'),
('Endurance'),
('Intrasonic'),
('Race Pro'),
('Race Speed'),
('Redline');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product_size`
--

CREATE TABLE `product_size` (
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product_size`
--

INSERT INTO `product_size` (`size`) VALUES
(142),
(147),
(152),
(157),
(162),
(167),
(172),
(177),
(182),
(187),
(192),
(197),
(202),
(207);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product_type`
--

CREATE TABLE `product_type` (
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product_type`
--

INSERT INTO `product_type` (`name`) VALUES
('classic'),
('double pole'),
('skate');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `shipment`
--

CREATE TABLE `shipment` (
  `nr` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `shipping_address` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `pickup_date` date NOT NULL,
  `driver_id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `company` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `team_skier_id` int(11) DEFAULT NULL,
  `individual_store_id` int(11) DEFAULT NULL,
  `franchise_id` int(11) DEFAULT NULL,
  `comment` varchar(2500) COLLATE utf8mb4_danish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `shipment`
--

INSERT INTO `shipment` (`nr`, `name`, `shipping_address`, `pickup_date`, `driver_id`, `state`, `company`, `team_skier_id`, `individual_store_id`, `franchise_id`, `comment`) VALUES
(1, 'Intersport', 'Hammersborggate 9, 0181 Oslo', '2022-04-21', 1, 'ready', 'Bring', NULL, NULL, 1, NULL),
(2, 'XXL Lysaker', 'Emanuels vei 12, 0283 Oslo\r\n', '2022-03-23', 2, 'picked up', 'Posten', NULL, 2, NULL, 'delivered successfully'),
(3, 'Petter Northug', 'Bestumveien 31, 0283 Oslo', '2022-04-30', 1, 'ready', 'Bring', 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `shipment_state`
--

CREATE TABLE `shipment_state` (
  `state` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `shipment_state`
--

INSERT INTO `shipment_state` (`state`) VALUES
('delivered'),
('picked up'),
('ready');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `storekeeper`
--

CREATE TABLE `storekeeper` (
  `nr` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `storekeeper`
--

INSERT INTO `storekeeper` (`nr`, `name`, `username`) VALUES
(1, 'Johanna Larsen', 'Johanna1'),
(2, 'Lars Hansen', 'Lars1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `team_skier`
--

CREATE TABLE `team_skier` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `shipping_address` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `dob` date NOT NULL,
  `club` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `team_skier`
--

INSERT INTO `team_skier` (`id`, `name`, `shipping_address`, `start_date`, `end_date`, `dob`, `club`, `username`) VALUES
(1, 'Marit Bjørgen', 'Bestumveien 32, 0283 Oslo', '2022-07-12', '2023-07-12', '1980-03-21', 'Landslaget', 'MaritBjorgen1'),
(2, 'Petter Northug', 'Bestumveien 31, 0283 Oslo', '2021-06-12', '2022-06-12', '1986-01-06', 'Landslaget', 'PetterNortug1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `transport_company`
--

CREATE TABLE `transport_company` (
  `name` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `transport_company`
--

INSERT INTO `transport_company` (`name`, `username`) VALUES
('Bring', 'Bring1'),
('Posten', 'Posten1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `transport_login`
--

CREATE TABLE `transport_login` (
  `username` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_danish_ci NOT NULL,
  `salt` varchar(10) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `transport_login`
--

INSERT INTO `transport_login` (`username`, `password`, `salt`) VALUES
('Bring1', '8aadf4eff8002295762288c757a3b4e1d744c4d9846a934c9c3da91acd345cab', '6TwbC'),
('Posten1', 'd1db94193a224e2ff3e73d8711cc778525c30db8cde82575e6202de5b9bb6037', 'iu7zF');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer_login`
--
ALTER TABLE `customer_login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `customer_rep`
--
ALTER TABLE `customer_rep`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `Customer_rep_Employee_login_fk` (`username`);

--
-- Indexes for table `employee_login`
--
ALTER TABLE `employee_login`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `franchise`
--
ALTER TABLE `franchise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Franchise_Customer_login_fk` (`username`);

--
-- Indexes for table `individual_store`
--
ALTER TABLE `individual_store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Individual_store_Franchise_fk` (`franchise_id`),
  ADD KEY `Individual_store_Customer_login_fk` (`username`);

--
-- Indexes for table `order_has`
--
ALTER TABLE `order_has`
  ADD PRIMARY KEY (`product_id`,`order_nr`),
  ADD KEY `Order_has_order_fk` (`order_nr`);

--
-- Indexes for table `order_skis`
--
ALTER TABLE `order_skis`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `Order_Franchise_fk` (`franchise_id`),
  ADD KEY `Order_Individual_store_fk` (`individual_store_id`),
  ADD KEY `Order_team_skier_fk` (`team_skier_id`),
  ADD KEY `Order_order_state_fk` (`state`),
  ADD KEY `Order_order_fk` (`reference_order_nr`),
  ADD KEY `Order_shippment_fk` (`shippment_nr`);

--
-- Indexes for table `order_state`
--
ALTER TABLE `order_state`
  ADD PRIMARY KEY (`state`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Product_Product_model_fk` (`model`),
  ADD KEY `Product_Product_type_fk` (`type`),
  ADD KEY `Product_Product_size_fk` (`size`);

--
-- Indexes for table `production_has`
--
ALTER TABLE `production_has`
  ADD PRIMARY KEY (`plan_id`,`product_id`),
  ADD KEY `Production_has_Product_fk` (`product_id`);

--
-- Indexes for table `production_plan`
--
ALTER TABLE `production_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_manager`
--
ALTER TABLE `product_manager`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `Product_manager_Employee_login_fk` (`username`);

--
-- Indexes for table `product_model`
--
ALTER TABLE `product_model`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`size`);

--
-- Indexes for table `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `shipment`
--
ALTER TABLE `shipment`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `Shipment_Shipment_state_fk` (`state`),
  ADD KEY `Shipment_Transport_company_fk` (`company`),
  ADD KEY `Shipment_Franchise_fk` (`franchise_id`),
  ADD KEY `Shipment_Individual_store_fk` (`individual_store_id`),
  ADD KEY `Shipment_Team_skier_fk` (`team_skier_id`);

--
-- Indexes for table `shipment_state`
--
ALTER TABLE `shipment_state`
  ADD PRIMARY KEY (`state`);

--
-- Indexes for table `storekeeper`
--
ALTER TABLE `storekeeper`
  ADD PRIMARY KEY (`nr`),
  ADD KEY `Storekeeper_Employee_login_fk` (`username`);

--
-- Indexes for table `team_skier`
--
ALTER TABLE `team_skier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Team_skier_Customer_login_fk` (`username`);

--
-- Indexes for table `transport_company`
--
ALTER TABLE `transport_company`
  ADD PRIMARY KEY (`name`),
  ADD KEY `Transport_company_Transport_login_fk` (`username`);

--
-- Indexes for table `transport_login`
--
ALTER TABLE `transport_login`
  ADD PRIMARY KEY (`username`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `customer_rep`
--
ALTER TABLE `customer_rep`
  ADD CONSTRAINT `Customer_rep_Employee_login_fk` FOREIGN KEY (`username`) REFERENCES `employee_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `franchise`
--
ALTER TABLE `franchise`
  ADD CONSTRAINT `Franchise_Customer_login_fk` FOREIGN KEY (`username`) REFERENCES `customer_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `individual_store`
--
ALTER TABLE `individual_store`
  ADD CONSTRAINT `Individual_store_Customer_login_fk` FOREIGN KEY (`username`) REFERENCES `customer_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Individual_store_Franchise_fk` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `order_has`
--
ALTER TABLE `order_has`
  ADD CONSTRAINT `Order_has_Product_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_has_order_fk` FOREIGN KEY (`order_nr`) REFERENCES `order_skis` (`nr`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `order_skis`
--
ALTER TABLE `order_skis`
  ADD CONSTRAINT `Order_Franchise_fk` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_Individual_store_fk` FOREIGN KEY (`individual_store_id`) REFERENCES `individual_store` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_order_fk` FOREIGN KEY (`reference_order_nr`) REFERENCES `order_skis` (`nr`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_order_state_fk` FOREIGN KEY (`state`) REFERENCES `order_state` (`state`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_shippment_fk` FOREIGN KEY (`shippment_nr`) REFERENCES `shipment` (`nr`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Order_team_skier_fk` FOREIGN KEY (`team_skier_id`) REFERENCES `team_skier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `Product_Product_model_fk` FOREIGN KEY (`model`) REFERENCES `product_model` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Product_Product_size_fk` FOREIGN KEY (`size`) REFERENCES `product_size` (`size`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Product_Product_type_fk` FOREIGN KEY (`type`) REFERENCES `product_type` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `production_has`
--
ALTER TABLE `production_has`
  ADD CONSTRAINT `Production_has_Product_fk` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Production_has_Production_plan_fk` FOREIGN KEY (`plan_id`) REFERENCES `production_plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `product_manager`
--
ALTER TABLE `product_manager`
  ADD CONSTRAINT `Product_manager_Employee_login_fk` FOREIGN KEY (`username`) REFERENCES `employee_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `shipment`
--
ALTER TABLE `shipment`
  ADD CONSTRAINT `Shipment_Franchise_fk` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Shipment_Individual_store_fk` FOREIGN KEY (`individual_store_id`) REFERENCES `individual_store` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Shipment_Shipment_state_fk` FOREIGN KEY (`state`) REFERENCES `shipment_state` (`state`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Shipment_Team_skier_fk` FOREIGN KEY (`team_skier_id`) REFERENCES `team_skier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Shipment_Transport_company_fk` FOREIGN KEY (`company`) REFERENCES `transport_company` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `storekeeper`
--
ALTER TABLE `storekeeper`
  ADD CONSTRAINT `Storekeeper_Employee_login_fk` FOREIGN KEY (`username`) REFERENCES `employee_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `team_skier`
--
ALTER TABLE `team_skier`
  ADD CONSTRAINT `Team_skier_Customer_login_fk` FOREIGN KEY (`username`) REFERENCES `customer_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Begrensninger for tabell `transport_company`
--
ALTER TABLE `transport_company`
  ADD CONSTRAINT `Transport_company_Transport_login_fk` FOREIGN KEY (`username`) REFERENCES `transport_login` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
